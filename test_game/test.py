import asyncio
import pygame, sys, random

import os

def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


pygame.init()

screen = pygame.display.set_mode((1280,720)) # contains width and height of window
clock = pygame.time.Clock() # must be capital C - creates clock object
pygame.mouse.set_visible(False) # hides the mouse cursor in the game


# VARIABLES:

wood_bg= pygame.image.load(resource_path('Wood_BG.png'))
land_bg = pygame.image.load(resource_path('Land_BG.png'))
water_bg = pygame.image.load(resource_path('Water_BG.png'))
cloud_1 = pygame.image.load(resource_path('Cloud1.png'))
cloud_2 = pygame.image.load(resource_path('Cloud2.png'))
crosshair = pygame.image.load(resource_path('crosshair.png'))
duck_surface = pygame.image.load(resource_path('duck.png'))

# wood_bg= pygame.image.load('images/Wood_BG.png')
# land_bg = pygame.image.load('images/Land_BG.png')
# water_bg = pygame.image.load('images/Water_BG.png')
# cloud_1 = pygame.image.load('images/Cloud1.png')
# cloud_2 = pygame.image.load('images/Cloud2.png')
# crosshair = pygame.image.load('images/crosshair.png')
# duck_surface = pygame.image.load('images/duck.png')


game_font = pygame.font.Font(None,75)
text_surface = game_font.render('YOU WON!', True, (255,255,255))
text_rect = text_surface.get_rect(center = (640, 360))
# RGB tuple: Red, Green, Blue: between 0 and 255

# POSITION VARIABLES:

land_position_y = 560
land_speed = 1

water_position_y = 640
water_speed = 1

# RECTANGLES:
# use rectangles to define the center of a surface
# and define the position of the surface based on the rectangle

# create the crosshair rect before the game starts:
# otherwise game will crash if player does not move the mouse
crosshair_rect = crosshair.get_rect(center = (640, 360))

duck_list = []
# create 20 duck rectangles
for i in range(20):
  duck_position_x = random.randrange(50,1200)
  duck_position_y = random.randrange(120, 600)
  duck_rect = duck_surface.get_rect(center = (duck_position_x, duck_position_y))
  duck_list.append(duck_rect)

# GAME LOOP:

while True:

  for event in pygame.event.get(): # pygame starts to look for events

    # to exit the while loop:
    if event.type == pygame.QUIT:
      # allows us to end the loop by quitting pygame
      # -> the X button at top of screen
      pygame.quit() # to close the pygame window - closes pygame
      sys.exit() # to quit without error message - closes program

    # to track the mouse position:
    if event.type == pygame.MOUSEMOTION:
      # event.pos or  # pygame.mouse.get_pos() # same thing
      # 1. we draw a rectangle around the image (surface)
      # 2. we place the center of the rectangle on the position of our mouse
      # 3. we blit the surface on the rectangle

      # this is so that the mouse position doesn't start in the top left corner
      # default 0, 0 position is top left corner, not center
      crosshair_rect = crosshair.get_rect(center = event.pos)
      # adding a rectangle around the surface image
      screen.blit(crosshair, event.pos)

    # to track the mouse click for collision
    if event.type == pygame.MOUSEBUTTONDOWN:
      for i, duck in enumerate(duck_list):
        # if crosshair_rect.colliderect(duck): # not precise enough
        if duck.collidepoint(event.pos): # makes the collision point more precise
          # check for collision, returns true or false
          # print(i, 'duck HIT')
          del duck_list[i] # deletes the duck that was hit


  # WOOD BG:

  # images load in order they are written:
  # furthest back layer must be first
  screen.blit(wood_bg, (0,0)) # wood background image and coordinates

  # LAND ANIMATION BG:

  # reverse the direction at a certain point of land_position_y:
  if land_position_y <= 540 or land_position_y >= 600:
    land_speed *= -1

  land_position_y -= land_speed

  screen.blit(land_bg, (0,land_position_y)) # land background: X = 0, Y = 560

  # DUCKS:

  for i in duck_list:
    screen.blit(duck_surface, i)

  # WATER ANIMATION BG:

  if water_position_y <= 620 or water_position_y >= 680:
    water_speed *= -1

  water_position_y += water_speed

  screen.blit(water_bg, (0, water_position_y))


  # CROSSHAIR:
  # before clouds, but after water and land
  # so will show up in front of water and land
  # but behind the clouds

  screen.blit(crosshair, crosshair_rect)

  # CLOUDS:

  screen.blit(cloud_1, (100,40))
  screen.blit(cloud_1, (280,60))

  screen.blit(cloud_1, (600,50))
  screen.blit(cloud_2, (475,40))

  screen.blit(cloud_1, (975,30))
  screen.blit(cloud_2, (850,50))

  # TEXT
  if len(duck_list) == 0:
    screen.blit(text_surface, text_rect)



  pygame.display.update() # keeps the screen going while True - careful for infinity loop
  clock.tick(120) # 120 frames per second - clock stops the game if it runs too fast
  # if game is too busy, it could run slow - must be designed in a way that is not too busy


# frame runs continuously
# speed is uncertain, we don't know the frame rate


# import image
# add it to display surface


# import clouds: cloud1 and cloud2
# place on top of screen


# collision mechanism:
# overlap + click = destroy duck
