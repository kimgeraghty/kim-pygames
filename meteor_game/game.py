import asyncio, pygame, sys, random, os

def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


# CLASSES:

# class for rectangles and surfaces
class SpaceShip(pygame.sprite.Sprite): # inherits from pygame.sprite.Sprite
  def __init__(self, path, x_position, y_position):
    super().__init__() # initialized all functionality from parent class

    self.uncharged = pygame.image.load(path)
    self.charged = pygame.image.load('spaceship_charged.png')

    self.image = self.uncharged # creates the surface
    self.rect = self.image.get_rect(center = (x_position, y_position))
    self.shield_surface = pygame.image.load('shield.png')
    self.health = 8 # number of lives

  def update(self):
    self.rect.center = pygame.mouse.get_pos()
    self.screen_constrain()
    self.display_health()

  # prevent spaceship from being cut off by screen:
  def screen_constrain(self):
    if self.rect.right >= 1280:
      self.rect.right = 1280
    if self.rect.left <= 0:
      self.rect.left = 0
    if self.rect.top <= 0:
      self.rect.top = 0
    if self.rect.bottom >= 720:
      self.rect.bottom = 720

  def display_health(self):
    for i in range(self.health):
      screen.blit(self.shield_surface, (10+50*i,10))

  def get_damage(self, damage_amount):
    self.health -= damage_amount

  def charge(self):
    self.image = self.charged

  def discharge(self):
    self.image = self.uncharged


class Meteor(pygame.sprite.Sprite):
  def __init__(self, path, x_pos, y_pos, x_speed, y_speed):
    super().__init__()
    self.image = pygame.image.load(path)
    self.rect = self.image.get_rect(center = (x_pos, y_pos))
    self.x_speed =  x_speed
    self.y_speed = y_speed
    self.x_pos =  x_pos
    self.y_pos = y_pos

  def update(self):
    self.rect.centerx += self.x_speed
    self.rect.centery += self.y_speed
    if self.rect.centery >= 800:
      self.kill() # destroy meteor once it goes past the screen limit


class Laser(pygame.sprite.Sprite):
  def __init__(self, path, pos, speed):
    super().__init__()
    self.image = pygame.image.load(path) # creates the surface
    self.rect = self.image.get_rect(center = (pos))
    self.speed = speed

  def update(self):
    self.rect.centery -= self.speed
    if self.rect.centery <= -100:
      self.kill() # deletes laser when it goes past the screen limit

# START PYGAME:

pygame.init()

screen = pygame.display.set_mode((1280,720))
# creates display surface
# contains width and height of window
clock = pygame.time.Clock() # must be capital C - creates clock object
game_font = pygame.font.Font(None,40)
# score = 0

# VARIABLES

spaceship = SpaceShip('spaceship.png', 640,500)
# creates with parent class SpaceShip
spaceship_group = pygame.sprite.GroupSingle()
spaceship_group.add(spaceship)

meteor_group = pygame.sprite.Group()
METEOR_EVENT = pygame.USEREVENT
pygame.time.set_timer(METEOR_EVENT, 100)  # 250 milliseconds (4 times per second)

laser_group = pygame.sprite.Group()


# pygame.mouse.set_visible(False) # hides the mouse cursor in the game

async def main():

  score = 0
  laser_timer = 0
  laser_active = False

  def main_game():

    # GAME LOGIC:

    laser_group.draw(screen)
    spaceship_group.draw(screen)
    meteor_group.draw(screen)

    spaceship_group.update()
    laser_group.update()
    meteor_group.update()

    # COLLISIONS:

    # spaceship and meteor:
    if pygame.sprite.spritecollide(spaceship_group.sprite, meteor_group, True):
      spaceship_group.sprite.get_damage(1)

    # laser and meteor:
    for laser in laser_group:
      pygame.sprite.spritecollide(laser, meteor_group, True)

    return 1


  def end_game():
    text_surface = game_font.render('GAME OVER! PRESS SPACE TO RESTART', True, (255,255,255))
    text_rect = text_surface.get_rect(center = (640, 320))
    screen.blit(text_surface, text_rect)

    score_surface = game_font.render(f'YOUR SCORE IS {score}', True, (255,255,255))
    score_rect = score_surface.get_rect(center = (640, 400))
    screen.blit(score_surface, score_rect)

  # VARIABLES:

  # RGB tuple: Red, Green, Blue: between 0 and 255

  # GAME LOOP:

  while True:

    for event in pygame.event.get(): # pygame starts to look for events

      # to exit the while loop:
      if event.type == pygame.QUIT:
        # allows us to end the loop by quitting pygame
        # -> the X button at top of screen
        pygame.quit() # to close the pygame window - closes pygame
        sys.exit() # to quit without error message - closes program

      if event.type == METEOR_EVENT:
        # print('METEOR')
        meteor_path = random.choice(('Meteor1.png','Meteor2.png','Meteor3.png'))
        meteor_x_pos = random.randrange(0,1280)
        meteor_y_pos = random.randrange(-500,-50)
        x_speed = random.randrange(-1,1)
        y_speed = random.randrange(4,10)

        meteor = Meteor(meteor_path,meteor_x_pos, meteor_y_pos, x_speed, y_speed)
        meteor_group.add(meteor)


      # shoot with laser
      if event.type == pygame.MOUSEBUTTONDOWN and laser_active:
        path = 'Laser.png'
        pos = pygame.mouse.get_pos()
        # pos = event.pos
        speed = 5
        laser = Laser(path, pos, speed)
        laser_group.add(laser)
        laser_active = False
        laser_timer = pygame.time.get_ticks() # tracks milliseconds
        spaceship_group.sprite.discharge()
        # print(laser_timer) # shows time when laser is shot

      # Laser time for recharge
      if pygame.time.get_ticks() - laser_timer >= 1000: # equals one second
        laser_active = True
        spaceship_group.sprite.charge()
        print('test', laser_active)


      # reset game logic:

      keys = pygame.key.get_pressed()

      if keys[pygame.K_SPACE] and spaceship_group.sprite.health <= 0:
      # if event.type == pygame.MOUSEBUTTONDOWN and spaceship_group.sprite.health <= 0:
        spaceship_group.sprite.health = 8 # restart the game
        meteor_group.empty() # get rid of any previous meteor groups
        score = 0

    screen.fill((42,45,51))
    # creates background color


    if spaceship_group.sprite.health > 0:
      score += main_game() # calls game logic function
      # print(score)
    else:
      end_game()


    pygame.display.update()
    # keeps the screen going while True - careful for infinity loop
     # draws the frame continuously

    clock.tick(120) # 120 frames per second - clock stops the game if it runs too fast
    # if game is too busy, it could run slow - must be designed in a way that is not too busy

    await asyncio.sleep(0)

asyncio.run(main())
# asyncio for running with wasm - file must be named main.py

# spritecollide(sprite,spritegroup,dokill)
