import asyncio
import pygame, sys, random

import os

def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


pygame.init()

screen = pygame.display.set_mode((1280,720)) # contains width and height of window
clock = pygame.time.Clock() # must be capital C - creates clock object
pygame.mouse.set_visible(False) # hides the mouse cursor in the game

async def main():

  # VARIABLES:

  # RGB tuple: Red, Green, Blue: between 0 and 255

  # GAME LOOP:

  while True:

    for event in pygame.event.get(): # pygame starts to look for events

      # to exit the while loop:
      if event.type == pygame.QUIT:
        # allows us to end the loop by quitting pygame
        # -> the X button at top of screen
        pygame.quit() # to close the pygame window - closes pygame
        sys.exit() # to quit without error message - closes program

    pygame.display.update() # keeps the screen going while True - careful for infinity loop
    clock.tick(120) # 120 frames per second - clock stops the game if it runs too fast
    # if game is too busy, it could run slow - must be designed in a way that is not too busy

    await asyncio.sleep(0)
  # frame runs continuously
  # speed is uncertain, we don't know the frame rate

  # collision mechanism:
  # overlap + click = destroy

asyncio.run(main())
# asyncio for running with wasm - file must be named main.py
